package main

import (
	"log"

	"gitlab.com/Gorteza71/compressor/compressor"
)

func main() {
	if err := compressor.ZipIt("/home/morteza/Desktop/ff/books/1.pdf", "/home/morteza/Desktop/myfilezip.zip"); err != nil {
		log.Println(err)
	}

	if err := compressor.UnZipIt("/home/morteza/Desktop/myfilezip.zip", "/home/morteza/Desktop/1"); err != nil {
		log.Println(err)
	}
}
